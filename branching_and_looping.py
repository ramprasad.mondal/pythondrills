def integers_from_start_to_end_using_range(start, end, step):
    """return a list"""
    # pass
    a = []
    for i in range(start, end, step):
        a.append(i)
    return a


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    # pass
    a = []
    # a.append(start)
    if step >= 0:
        while start < end:
            a.append(start)
            start += step
            
    else:
        while start > end:
            a.append(start)
            start += step
            
    return a




def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    # pass
    a = []
    for i in range(10,99):
        count = 0
        for j in range(2,i-1):
            if i%j == 0:
                count += 1
        if count == 0:
            a.append(i)
    return a

