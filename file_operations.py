"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    with open(path, mode='r') as file:
        s = file.read()    
    return s
    # f = open(path, mode= 'r')
    # s = f.read()
    # f.close()
    # return s

def write_to_file(path, s):
    with open(path, mode='w') as file:
        file.write(s)
    # f = open(path, mode= 'w')
    # s = f.write(s)
    # f.close()


def append_to_file(path, s):
    with open(path, mode='a') as file:
        file.write(s)
    # f = open(path, mode= 'a')
    # s = f.write(s)
    # f.close()


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in 
    the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    for i in range(1,n+1):
        with open(file_path, mode='a') as file:
            file.write("{},{}\n".format(i,i*i))
