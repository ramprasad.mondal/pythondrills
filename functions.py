def is_prime(n):
    """
    Check whether a number is prime or not
    """
    count = 0
    for i in range(2,n-1):
        if n%i == 0:
            count += 1
    if count == 0:
        return True
    else:
        return False


def n_digit_primes(digit = 2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    if digit == 2:
        a = []
        for i in range(10,99):
            count = 0
            for j in range(2,i-1):
                if i%j == 0:
                    count += 1
            if count == 0:
                a.append(i)
        return a
    
    else:
        a = []
        for i in range(2,9):
            count = 0
            for j in range(2,i-1):
                if i%j == 0:
                    count += 1
            if count == 0:
                a.append(i)
        return a