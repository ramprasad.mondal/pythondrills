def html_dict_search(html_dict, selector):
    """
    Implement `id` and `class` selectors
    """
    reqList = []

    if selector[0] == ".":
        search = "class"
    if selector[0] == "#":
        search = "id"
    value = selector[1:]

    def find_selector(child, search):
        if search in child["attrs"]:
            if child["attrs"][search] == value:
                reqList.append(child)
        for child in child["children"]:
            find_selector(child, search)

    for child in html_dict["children"]:
        find_selector(child, search)

    return reqList
