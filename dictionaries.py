def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    s = s.replace(',', '')
    s = s.replace('.', '')
    s = s.split()
    a = dict()
    for i in range(len(s)):
        if s[i] in a:
            a[s[i]] += 1
        else:
            a[s[i]] = 1
    return a


def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    a = []
    for key, value in d.items():
        a.append((key,value))
    return a


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    return sorted(d.items())
    # b = []
    # for i in a:
    #     b.append((i,d[i]))
    # return b
